package main

import (
	"REST" // other .go files
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("$PORT must be set")
	}
	http.HandleFunc("/conservation/v1/country/", REST.HandlerCountry)
	http.HandleFunc("/conservation/v1/species/", REST.HandlerSpecies)
	http.HandleFunc("/conservation/v1/diag/", REST.HandlerDiag)

	//log.Fatal(http.ListenAndServe("0.0.0.0", nil))
	fmt.Println("Listening on port " + port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
