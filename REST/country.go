package REST // other .go files

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
)

type Country struct {
	Code       string   `json:"alpha2Code"`
	Name       string   `json:"name"`
	Flag       string   `json:"flag"`
	Species    []string `json:"species"`
	SpeciesKey []int    `json:"speciesKey"`
}

type Speciesid struct {
	Results []struct {
		Species    string `json:"species"`
		SpeciesKey int    `json:"speciesKey"`
	} `json:"results"`
}

// get somthing back without error? Get http -> error? -> struct
func recieveAPI(w http.ResponseWriter, url string) io.Reader {
	response, err := http.Get(url)
	if err != nil {
		http.Error(w, "Could not request server", http.StatusNotFound)
	}
	return response.Body
}

// func check if equal to s // c == s
func (c *Country) checkDuplicate(s string) bool {
	for _, checkS := range c.Species {
		if checkS == s {
			return true
		}
	}
	return false
}

//make it look nice. spaces /n symbol...
func (co Country) format(w http.ResponseWriter) {
	json_co, err := json.MarshalIndent(co, "", "    ")
	if err != nil {
		http.Error(w, "Could not encode the object", http.StatusBadRequest)
	}
	fmt.Fprint(w, string(json_co)) // print everything
}

func HandlerCountry(w http.ResponseWriter, r *http.Request) {
	parts := strings.Split(r.URL.Path, "/") // http.
	country_identifier := parts[4]          // one part
	limit := parts[5]                       // number of vari
	//http://localhost:8080/conservation/v1/country/co/2222
	//0                     1            2  3       4  5

	//country_identifier = co?
	url_country := "https://restcountries.eu/rest/v2/alpha/" + country_identifier
	url_species := "https://api.gbif.org/v1/occurrence/search?country=" + country_identifier + "&limit=" + limit

	var co Country
	var sp Speciesid

	err := json.NewDecoder(recieveAPI(w, url_country)).Decode(&co)
	if err != nil {
		http.Error(w, "Could not decode the response from client | country", http.StatusBadRequest)
	}
	err = json.NewDecoder(recieveAPI(w, url_species)).Decode(&sp)
	if err != nil {
		http.Error(w, "Could not decode the response from client | limit", http.StatusBadRequest)
	}
	for _, i := range sp.Results { // limit && less or = to max
		if co.checkDuplicate(i.Species) != true { // checker if clones
			co.Species = append(co.Species, i.Species)          // species   ["1",2,3]
			co.SpeciesKey = append(co.SpeciesKey, i.SpeciesKey) // speciesKey["1",2,3]
		}
	}

	co.format(w)

}
