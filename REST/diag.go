package REST

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"
)

// Diagnostics
type Diag struct {
	Gbif          int     `json:"gbif"`
	Restcountries int     `json:"restcountries"`
	Version       string  `json:"version"`
	Uptime        float64 `json:"uptime"`
}

type Time struct {
	StartTime time.Time
	Uptime    float64
}

// timer from timer // Diag
func (t *Time) getStatusAndTimer(w http.ResponseWriter, url string) int {

	result, err := http.Get(url) // get http
	if err != nil {
		http.Error(w, "Could not retrieve from server", http.StatusBadRequest)
		t.StartTime = time.Now()
	}
	t.Uptime = time.Since(t.StartTime).Seconds() // curtime - startime // time res
	return result.StatusCode                     // 200 // not 200 ok
}

//make it look nice. spaces /n symbol...
func (dg Diag) format(w http.ResponseWriter) {
	json_dg, err := json.MarshalIndent(dg, "", "  ")
	if err != nil {
		http.Error(w, "Could not encode object", http.StatusBadRequest)
	}
	fmt.Fprint(w, string(json_dg))
}

func HandlerDiag(w http.ResponseWriter, r *http.Request) {

	timer := Time{
		StartTime: time.Now(), // ""get cur time"""
	}

	parts := strings.Split(r.URL.Path, "/")
	version := parts[2] // v1

	url_gbif := "https://api.gbif.org/v1/species/"
	url_rest_countries := "https://restcountries.eu/rest/v2/"

	var dg Diag

	dg.Gbif = timer.getStatusAndTimer(w, url_gbif)                    // = status 200
	dg.Restcountries = timer.getStatusAndTimer(w, url_rest_countries) // time from status
	dg.Version = version                                              // = http... v1
	dg.Uptime = timer.Uptime                                          // time from start.// last ok request // x1 or x2

	dg.format(w)

}
