package REST // other .go files

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
)

type Species struct {
	Key            int    `json:"key"`
	Kingdom        string `json:"kingdom"`
	Phylum         string `json:"phylum"`
	Order          string `json:"order"`
	Family         string `json:"family"`
	Genus          string `json:"genus"`
	ScientificName string `json:"scientificName"`
	CanonicalName  string `json:"canonicalName"`
	Year           string `json:"bracketYear"`
}

type Year struct {
	Number string `json:"year"`
}

//make it look nice. spaces /n symbol...
func (s Species) format(w http.ResponseWriter) {
	json_sp, err := json.MarshalIndent(s, "", "    ")
	if err != nil {
		http.Error(w, "Could not encode object", http.StatusBadRequest)
	}
	fmt.Fprint(w, string(json_sp)) // prints struct to postman
}

// w give info, r read info || w http, r handlerspecies
func HandlerSpecies(w http.ResponseWriter, r *http.Request) {
	// splits on every “/”
	parts := strings.Split(r.URL.Path, "/")                  // http
	key := parts[4]                                          // Key
	url_sp := "https://api.gbif.org/v1/species/" + key + "/" // species
	url_year := "https://api.gbif.org/v1/species/" + key + "/name/"

	var sp Species
	//     json. = as json type
	//          NewDecoder(r reader).decode = one func // NewDecoder = err
	//                     r reader ""emty vari""
	//                                            json.Decode(&sp) sp = json as go data
	err := json.NewDecoder(recieveAPI(w, url_sp)).Decode(&sp)
	if err != nil {
		http.Error(w, "Could not decode response from client | species", http.StatusBadRequest)
	}
	// recieveAPI = get massage. err = does it work?
	// recieveAPI return struct. // resp.body from get.http...
	err = json.NewDecoder(recieveAPI(w, url_year)).Decode(&sp)
	if err != nil {
		http.Error(w, "Could not decode response from client | year", http.StatusBadRequest)
	}
	sp.format(w)

}
